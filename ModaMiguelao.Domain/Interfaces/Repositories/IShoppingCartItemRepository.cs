﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Repositories
{
    public interface IShoppingCartItemRepository : IRepositoryBase<ShoppingCartItem>
    {
    }
}
