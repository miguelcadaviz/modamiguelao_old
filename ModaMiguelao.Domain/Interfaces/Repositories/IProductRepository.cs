﻿using ModaMiguelao.Domain.Entities;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Interfaces.Repositories
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        IEnumerable<Product> GetByName(string name);
    }
}
