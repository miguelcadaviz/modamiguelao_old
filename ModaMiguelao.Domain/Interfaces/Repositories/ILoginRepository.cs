﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Repositories
{
    public interface ILoginRepository : IRepositoryBase<Login>
    {
    }
}
