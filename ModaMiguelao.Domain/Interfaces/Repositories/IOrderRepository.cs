﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Repositories
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
    }
}

