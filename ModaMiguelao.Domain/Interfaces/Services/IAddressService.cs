﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IAddressService : IServiceBase<Address>
    {
    }
}
