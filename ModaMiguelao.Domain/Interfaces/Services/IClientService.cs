﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IClientService : IServiceBase<Client>
    {
        //IEnumerable<Client> GetSpecialClients(IEnumerable<Client> clients);
    }
}
