﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface ILoginService : IServiceBase<Login>, ILoginServiceBase
    {
    }

    //TODO dar uma olhada nisso
    public interface ILoginServiceBase
    {
    }
}
