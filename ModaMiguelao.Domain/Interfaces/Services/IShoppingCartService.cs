﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IShoppingCartService : IServiceBase<ShoppingCart>
    {
    }
}
