﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IOrderService : IServiceBase<Order>
    {
    }
}
