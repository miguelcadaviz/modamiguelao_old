﻿using System;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity item);
        void Update(TEntity item);
        void Remove(TEntity item);
        TEntity GetById(long id);
        IEnumerable<TEntity> GetAll();
    }
}
