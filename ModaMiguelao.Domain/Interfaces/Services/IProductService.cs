﻿using ModaMiguelao.Domain.Entities;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IProductService : IServiceBase<Product>
    {
        IEnumerable<Product> GetByName(string name);
    }
}
