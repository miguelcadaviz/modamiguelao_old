﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Domain.Interfaces.Services
{
    public interface IAdministratorService : IServiceBase<Administrator>, ILoginServiceBase
    {
    }
}
