﻿using ModaMiguelao.Domain.Interfaces.Services;
using ModaMiguelao.Domain.Services;
using SimpleInjector;

namespace ModaMiguelao.Domain.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            /** Injeta as dependencias desse nível **/
            container.Register(typeof(IServiceBase<>), typeof(ServiceBase<>), Lifestyle.Scoped);
            container.Register<IClientService, ClientService>(Lifestyle.Scoped);
            container.Register<IProductService, ProductService>(Lifestyle.Scoped);
            container.Register<IAddressService, AddressService>(Lifestyle.Scoped);
            container.Register<IOrderService, OrderService>(Lifestyle.Scoped);
            container.Register<IShoppingCartItemService, ShoppingCartItemService>(Lifestyle.Scoped);
            container.Register<IShoppingCartService, ShoppingCartService>(Lifestyle.Scoped);
            container.Register<ILoginService, LoginService>(Lifestyle.Scoped);
            container.Register<IAdministratorService, AdministratorService>(Lifestyle.Scoped);

            /** Injeta as dependencias das outras camadas **/
        }
    }
}
