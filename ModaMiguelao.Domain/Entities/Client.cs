﻿using System;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Entities
{
    public class Client
    {
        #region Properties
        public long Id { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public DateTime ClientSince { get; set; }
        public bool Active { get; set; }

        //public virtual IEnumerable<Product> Products { get; set; }

        //public bool SpecialClient(Client client)
        //{
        //    return client.Active && DateTime.Now.Year - client.ClientSince.Year >= 5;
        //}
        #region Navigation Properties
        public virtual IEnumerable<Address> Addresses { get; set; }
        #endregion
        #endregion


        #region Constructors
        public Client()
        {
            //Addresses = new List<Address>();
        }
        #endregion

        #region Methods
        #endregion
    }
}
