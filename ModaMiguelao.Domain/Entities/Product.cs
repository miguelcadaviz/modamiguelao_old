﻿namespace ModaMiguelao.Domain.Entities
{
    public class Product
    {
        #region Properties
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }

        public string Category { get; set; }

        public bool Available { get; set; }

        //public long ClientId { get; set; }
        //public virtual Client Client { get; set; }
        #endregion
        
        #region Constructors
        #endregion

        #region Methods
        #endregion
    }
}
