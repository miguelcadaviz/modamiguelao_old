﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ModaMiguelao.Domain.Entities
{
    public class ShoppingCart
    {
        #region Properties
        public long Id { get; set; }
        public List<ShoppingCartItem> Itens { get; }

        public decimal Total
        {
            get
            {
                return this.Itens.Sum(x => x.Value);
            }
        }

        #region Navigation Properties
        public long OrderId { get; set; }
        public virtual Order Order { get; set; }
        #endregion
        #endregion

        #region Constructors
        public ShoppingCart()
        {
            this.Itens = new List<ShoppingCartItem>();
        }
        #endregion

        #region Methods
        public void AddItem(Product product, int quantity = 1)
        {
            if (quantity < 1)
                throw new Exception("Quantidade não pode ser negativa!");

            if (this.Itens.Any(x => x.Product.Id == product.Id))
            {
                //throw new Exception("Produto já inserido no carrinho!");
                this.Itens.Where(x => x.Product.Id == product.Id).FirstOrDefault().Quantity += quantity;
            }
            else {
                this.Itens.Add(new ShoppingCartItem(product: product, quantity: quantity));
            }
        }

        public void RemoveItem(ShoppingCartItem shoppingCartItem)
        {
            //if (this.Itens.Any(x => x.Product.Id != product.Id))
            //    throw new Exception("Produto não localizado!");

            //this.Itens.RemoveAll(x => x.Product.Id == product.Id);
            this.Itens.Remove(shoppingCartItem);
        }

        public void Clear()
        {
            this.Itens.Clear();
        }
        #endregion
    }
}
