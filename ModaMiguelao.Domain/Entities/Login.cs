﻿using System;

namespace ModaMiguelao.Domain.Entities
{
    public class Login
    {
        #region Properties
        public long Id { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public DateTime LastAccess { get; set; }
        public bool Active { get; set; }

        #region Navigation Properties
        #endregion
        #endregion


        #region Constructors
        //public Login()
        //{
        //}
        #endregion

        #region Methods
        #endregion
    }
}