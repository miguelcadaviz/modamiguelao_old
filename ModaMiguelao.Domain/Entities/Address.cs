﻿using System.ComponentModel;

namespace ModaMiguelao.Domain.Entities
{
    public class Address
    {
        #region Properties
        public long Id { get; set; }
        public EnumAddressType AddressType { get; set; }
        public string Description { get; set; }
        public string ZipCode { get; set; }
        public string Street { get; set; }
        public uint Number { get; set; }
        public string Complement { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public EnumBrazilianStates State { get; set; }
        public string Country { get; set; }

        #region Navigation Properties
        public long ClientId { get; set; }
        public virtual Client Client { get; set; }

        //public long OrderId { get; set; }
        //public virtual Client Order { get; set; }
        #endregion
        #endregion

        #region Constructors
        #endregion

        #region Methods
        #endregion
    }

    #region Enums
    public enum EnumAddressType
    {
        Residential = 0,
        Comercial = 1,
        Other = 2,
    }

    public enum EnumBrazilianStates
    {
        AC,
        AL,
        AP,
        AM,
        BA,
        CE,
        DF,
        ES,
        GO,
        MA,
        MT,
        MS,
        MG,
        PA,
        PB,
        PR,
        PE,
        PI,
        RJ,
        RN,
        RS,
        RO,
        RR,
        SC,
        SP,
        SE,
        TO,
    }
    #endregion
}
