﻿namespace ModaMiguelao.Domain.Entities
{
    public class ShoppingCartItem
    {
        #region Properties
        public long Id { get; set; }
        public Product Product { get; private set; }
        public int Quantity { get; set; }
        public decimal Value
        {
            get
            {
                return Product.Value * Quantity;
            }
        }

        #region Navigation Properties
        public long ShoppingCartId { get; set; }
        public virtual ShoppingCart ShoppingCart { get; set; }
        #endregion
        #endregion

        #region Constructor
        public ShoppingCartItem(Product product, int quantity)
        {
            this.Product = product;
            this.Quantity = quantity;
        }
        #endregion

        #region Methods
        #endregion
    }
}
