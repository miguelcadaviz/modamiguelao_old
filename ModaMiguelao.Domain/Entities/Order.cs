﻿namespace ModaMiguelao.Domain.Entities
{
    public class Order
    {
        #region Properties
        public long Id { get; set; }

        #region Navigation Properties
        public long ClientId { get; set; }
        public virtual Client Client { get; set; }

        public long ShoppingCartId { get; set; }
        public virtual ShoppingCart ShoppingCart { get; set; }

        public long DeliveryAddressId { get; set; }
        public virtual Address DeliveryAddress { get; set; }
        #endregion
        #endregion

        #region Constructors
        #endregion

        #region Methods
        #endregion
    }
}
