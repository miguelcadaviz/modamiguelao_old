﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
    internal class ShoppingCartItemService : ServiceBase<ShoppingCartItem>, IShoppingCartItemService
    {
        #region Properties
        private IShoppingCartItemRepository ShoppingCartItemRepository { get; }
        #endregion

        #region Constructors
        public ShoppingCartItemService(IShoppingCartItemRepository shoppingCartItemRepository) : base(shoppingCartItemRepository)
        {
            ShoppingCartItemRepository = shoppingCartItemRepository;
        }
        #endregion

        #region Methods
        #region IShoppingCartItemService implementation
        #endregion
        #endregion
    }
}
