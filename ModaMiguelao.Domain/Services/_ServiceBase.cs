﻿using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Services
{
    internal class ServiceBase<TEntity> : IServiceBase<TEntity>, IDisposable where TEntity : class
    {
        #region Properties
        private IRepositoryBase<TEntity> Repository { get; }
        #endregion

        #region Constructors
        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            Repository = repository;
        }
        #endregion

        #region Methods
        #region IServiceBase implementation
        public void Add(TEntity item)
        {
            Repository.Add(item);
        }
        public void Update(TEntity item)
        {
            Repository.Update(item);
        }

        public void Remove(TEntity item)
        {
            Repository.Remove(item);
        }

        public TEntity GetById(long id)
        {
            return Repository.GetById(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }
        #endregion

        #region IDisposable implementation
        public void Dispose()
        {
            Repository.Dispose();
        }
        #endregion
        #endregion
    }
}
