﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
    internal class ShoppingCartService : ServiceBase<ShoppingCart>, IShoppingCartService
    {
        #region Properties
        private IShoppingCartRepository ShoppingCartRepository { get; }
        #endregion

        #region Constructors
        public ShoppingCartService(IShoppingCartRepository shoppingCartRepository) : base(shoppingCartRepository)
        {
            ShoppingCartRepository = shoppingCartRepository;
        }
        #endregion

        #region Methods
        #region IShoppingCartService implementation
        #endregion
        #endregion
    }
}
