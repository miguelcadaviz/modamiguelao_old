﻿using System;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
   internal class LoginService : ServiceBase<Login>, ILoginService
    {
        #region Properties
        private ILoginRepository LoginRepository { get; }
        #endregion

        #region Constructors
        public LoginService(ILoginRepository loginRepository) : base(loginRepository)
        {
            LoginRepository = loginRepository;
        }
        #endregion

        #region Methods
        #region ILoginService implementation
        #endregion
        #endregion
    }
}
