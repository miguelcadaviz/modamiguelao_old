﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
    internal class AddressService : ServiceBase<Address>, IAddressService
    {
        #region Properties
        private IAddressRepository AddressRepository { get; }
        #endregion

        #region Constructors
        public AddressService(IAddressRepository addressRepository) : base(addressRepository)
        {
            AddressRepository = addressRepository;
        }
        #endregion

        #region Methods
        #region IAddressService implementation
        #endregion
        #endregion
    }
}
