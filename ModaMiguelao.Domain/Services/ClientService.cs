﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
    internal class ClientService : ServiceBase<Client>, IClientService
    {
        #region Properties
        private IClientRepository ClientRepository { get; }
        #endregion

        #region Constructors
        public ClientService(IClientRepository clientRepository) : base(clientRepository)
        {
            ClientRepository = clientRepository;
        }
        #endregion

        #region Methods
        #region IClientService implementation
        #endregion
        #endregion

        //public IEnumerable<Client> GetSpecialClients(IEnumerable<Client> clients)
        //{
        //    //return clients.Where(client => client.SpecialClient(client));
        //    return null;
        //}
    }
}
