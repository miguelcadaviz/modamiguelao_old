﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Services
{
    internal class ProductService : ServiceBase<Product>, IProductService
    {
        #region Properties
        private IProductRepository ProductRepository { get; }
        #endregion

        #region Constructors
        public ProductService(IProductRepository productRepository) : base(productRepository)
        {
            ProductRepository = productRepository;
        }
        #endregion

        #region Methods
        #region IProductService implementation
        public IEnumerable<Product> GetByName(string name)
        {
            return ProductRepository.GetByName(name);
        }
        #endregion
        #endregion

    }
}
