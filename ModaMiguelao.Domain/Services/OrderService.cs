﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
    internal class OrderService : ServiceBase<Order>, IOrderService
    {
        #region Properties
        private IOrderRepository OrderRepository { get; }
        #endregion

        #region Constructors
        public OrderService(IOrderRepository orderRepository) : base(orderRepository)
        {
            OrderRepository = orderRepository;
        }
        #endregion

        #region Methods
        #region IOrderService implementation
        #endregion
        #endregion
    }
}
