﻿using System;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Domain.Services
{
    internal class AdministratorService : ServiceBase<Administrator>, IAdministratorService 
    {
        #region Properties
        private IAdministratorRepository AdministratorRepository { get; }
        #endregion

        #region Constructors
        public AdministratorService(IAdministratorRepository administratorRepository) : base(administratorRepository)
        {
            AdministratorRepository = administratorRepository;
        }
        #endregion

        #region Methods
        #region IAdministratorService implementation
        #endregion
        #endregion
    }
}
