﻿using System;

namespace ModaMiguelao.Web.PartialControllers
{
    public class PagingController
    {
        public int TotalItens { get; set; }

        //TODO - webconfig
        public int ItensPerPage { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((decimal)this.TotalItens / this.ItensPerPage);
            }
        }
    }
}