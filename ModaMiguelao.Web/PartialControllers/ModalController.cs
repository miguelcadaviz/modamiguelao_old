﻿namespace ModaMiguelao.Web.PartialControllers
{
    public class ModalController
    {
        public string Title { get; set; }
        public string BodyText { get; set; }

        public bool DisplayOkButton { get; set; } = false;
        public bool DisplayYesNoButton { get; set; } = true;
    }

    public class RemoveShoppingCartItemModalController : ModalController
    {
        public RemoveShoppingCartItemModalController()
        {
            base.Title = "Remover item";
            base.BodyText = "Tem certeza que deseja remover o item do carrinho?";
        }
    }
}
