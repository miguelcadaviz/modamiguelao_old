﻿using System.ComponentModel;

namespace ModaMiguelao.Web.ViewModels
{
    public class AddressViewModel
    {
        public long Id { get; set; }
        public EnumAddressTypeViewModel AddressType { get; set; }
        public string Description { get; set; }
        public string ZipCode { get; set; }
        public string Street { get; set; }
        public uint Number { get; set; }
        public string Complement { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public EnumBrazilianStatesViewModel State { get; set; }
    }

    #region Enums
    public enum EnumAddressTypeViewModel
    {
        [Description("Residencial")]
        Residential = 0,
        [Description("Comercial")]
        Comercial = 1,
        [Description("Outro")]
        Other = 2,
    }

    public enum EnumBrazilianStatesViewModel
    {
        [Description("Acre")]
        AC,
        [Description("Alagoas")]
        AL,
        [Description("Amapá")]
        AP,
        [Description("Amazonas")]
        AM,
        [Description("Bahia")]
        BA,
        [Description("Ceará")]
        CE,
        [Description("Destrito Federal")]
        DF,
        [Description("Espírito Santo")]
        ES,
        [Description("Goiás")]
        GO,
        [Description("Maranhão")]
        MA,
        [Description("Mato Grosso")]
        MT,
        [Description("Mato Grosso do Sul")]
        MS,
        [Description("Minas Gerais")]
        MG,
        [Description("Pará")]
        PA,
        [Description("Paraíba")]
        PB,
        [Description("Paraná")]
        PR,
        [Description("Pernambuco")]
        PE,
        [Description("Piauí")]
        PI,
        [Description("Rio de Janeiro")]
        RJ,
        [Description("Rio Grande do Norte")]
        RN,
        [Description("Rio Grande do Sul")]
        RS,
        [Description("Rondônia")]
        RO,
        [Description("Roraima")]
        RR,
        [Description("Santa Catarina")]
        SC,
        [Description("São Paulo")]
        SP,
        [Description("Sergipe")]
        SE,
        [Description("Tocantins")]
        TO,
    }
    #endregion
}