﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ModaMiguelao.Web.ViewModels
{
    public class AdministratorViewModel
    {
        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "Preencha o campo nome")]
        [MaxLength(30, ErrorMessage = "Máximo {0} caracteres")]
        [MinLength(2, ErrorMessage = "Mínimo {0} caracteres")]
                public string User { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Preencha o campo nome")]
        [MaxLength(150, ErrorMessage = "Máximo {0} caracteres")]
        [MinLength(6, ErrorMessage = "Mínimo {0} caracteres")]
        public string Password { get; set; }

        public DateTime LastAccess { get; set; }
        public bool Active { get; set; }


    }
}