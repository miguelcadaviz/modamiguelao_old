﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ModaMiguelao.Web.ViewModels
{
    public class ProductViewModel
    {
        [Key]
        public long Id { get; set; }

        //TODO - tamanho maximo

        [DisplayName("Nome")]
        [Required(ErrorMessage = "Preencha o campo nome")]
        [MaxLength(250, ErrorMessage = "Máximo {0} caracteres")]
        [MinLength(2, ErrorMessage = "Mínimo {0} caracteres")]
        public string Name { get; set; }

        [DisplayName("Descrição")]
        [Required(ErrorMessage = "Preencha o campo Descrição")]
        [DataType(DataType.MultilineText)]
        [MaxLength(100, ErrorMessage = "Máximo {0} caracteres")]
        public string Description { get; set; }

        [DisplayName("Valor")]
        [DataType(DataType.Currency)]
        [Range(0.01, double.MaxValue, ErrorMessage ="Valor fora do intervalo")]
        [Required(ErrorMessage = "Preencha o campo valor")]
        public decimal Value { get; set; }

        [Required(ErrorMessage = "Preencha o campo Categoria")]
        [DisplayName("Categoria")]
        public string Category { get; set; }

        [DisplayName("Disponível?")]
        public bool Available { get; set; }

        [Required(ErrorMessage = "Preencha o campo Imagem")]
        [DisplayName("Imagem")]
        public string ImagePath { get; set; }

        public HttpPostedFileBase Image { get; set; }


        //public long ClientId { get; set; }
        //public virtual ClientViewModel Client { get; set; }
    }
}