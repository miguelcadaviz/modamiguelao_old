﻿using ModaMiguelao.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ModaMiguelao.Web.ViewModels
{
    public class ShoppingCartItemViewModel
    {
        public ProductViewModel Product { get; set; }
        public int Quantity { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(decimal), "0", "999999999999")]
        public decimal Value { get; set; }
    }
}