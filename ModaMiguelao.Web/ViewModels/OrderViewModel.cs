﻿namespace ModaMiguelao.Web.ViewModels
{
    public class OrderViewModel
    {
        public ClientViewModel Client { get; set; }
        public ShoppingCartViewModel ShoppingCart { get; set; }
        public AddressViewModel DeliveryAddress { get; set; }


        public bool Wrap { get; set; }
    }
}