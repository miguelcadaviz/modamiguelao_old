﻿function Modal() {

    //Propriedades da modal
    var ModalOK;
    var ModalYes;
    var ModalNo;
    
    //Métodos Sets
    this.SetNoFunction = function (value) {
        ModalNo = value;
    }
    this.SetYesFunction = function (value) {
        ModalYes = value;
    }
    this.SetOkFunction = function (value) {
        ModalOk = value;
    }

    //Métodos
    this.open = function () {
        $("#myModal").modal('show');
    }

    this.close = function () {
        $("#myModal").modal('hide');
    }

    this.startLoading = function () {
        var loading = "<span><em>Processando...</em> <i class='glyphicon glyphicon-refresh icon-refresh-animate'></i></span>";
        $('#myModal .modal-header h4').after(loading);
    }

    this.noClick = function () {
        ModalNo();
    }

    this.yesClick = function () {
        ModalYes();
    }

    this.okClick = function () {
        ModalOK();
    }
}