﻿function removeShoppingCartItem(itemId) {
    removedId = itemId;

    modal = new Modal();
    modal.SetNoFunction(modalNo);
    modal.SetYesFunction(modalYes);
    modal.open();
}

function modalYes() {
    debugger;
    var data = { id: removedId };
    var successCallback = function () {
        removeRow(removedId);
        modal.close();
    };
    var errorCallback = function () {
        modal.close();
    };
    var beforeSendCallback = function () {
        modal.startLoading();
    };
    var completeCallback = function () {
    };

    //TODO migrar para angular
    //TODO verificar esse ajax ae
    callAjax(urlRemoveitem, "", "", data, successCallback, errorCallback, beforeSendCallback, completeCallback);
}

function modalNo(removedId) {
}

function removeRow(removedId) {
    var rowId = '#row-' + removedId;

    $(rowId).animate({ opacity: 0.0 }, 400, function () {
        $(rowId).remove();
    });
}