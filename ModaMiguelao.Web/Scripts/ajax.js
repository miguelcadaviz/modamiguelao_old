﻿
    //function callAjax(url, type, dataType, data, successCallback, errorCallback) {
    //    callAjax(url, type, dataType, data, successCallback, errorCallback, null, null);
    //}

    function callAjax(url, type, dataType, data, successCallback, errorCallback, beforeSendCallback, completeCallback) {
        $.ajax({
            url: url,
            async: true,
            type: 'post',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                beforeSendCallback();
            },
            complete: function () {
                completeCallback();
            },
            success: function () {
                successCallback();
            },
            error: function () {
                errorCallback();
            }
        });
    }

    var ajaxType = {
        post: { value: 0, name: "post" },
        get: { value: 1, name: "get" }
    };

    var ajaxDataType = {
        json: { value: 0, name: "json" },
    };
