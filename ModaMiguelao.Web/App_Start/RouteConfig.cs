﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ModaMiguelao.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: null,
                url: "Product/{category}/Pagina{page}",
                defaults: new { controller = "Product", action = "Index", category = string.Empty, page = @"\d+"},
                namespaces: new[] { "ModaMiguelao.Web.Controllers" }
            );

            routes.MapRoute(
                name: null,
                url: "Product/{category}/",
                defaults: new { controller = "Product", action = "Index", category = string.Empty },
                namespaces: new[] { "ModaMiguelao.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ModaMiguelao.Web.Controllers" }
            );
        }
    }
}
