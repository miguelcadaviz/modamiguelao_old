﻿using AutoMapper;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.ViewModels;

namespace ModaMiguelao.Web.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "ViewModelToDomainMappings";
            }
        }

        protected override void Configure()
        {
            CreateMap<ClientViewModel, Client>();
            CreateMap<ProductViewModel, Product>();
            CreateMap<ShoppingCartViewModel, ShoppingCart>();
            CreateMap<ShoppingCartItemViewModel, ShoppingCartItem>();
            CreateMap<OrderViewModel, Order>();
            CreateMap<AddressViewModel, Address>();
            CreateMap<AdministratorViewModel, Administrator>();
        }
    }
}