﻿using AutoMapper;

namespace ModaMiguelao.Web.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDomainMappingProfile>();

                //x.AddProfile(new DomainToViewModelMappingProfile());
                //x.AddProfile(new ViewModelToDomainMappingProfile());
            });
        }
    }
}