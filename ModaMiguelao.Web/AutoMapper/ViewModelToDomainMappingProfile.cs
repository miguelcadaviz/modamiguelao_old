﻿using AutoMapper;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.ViewModels;

namespace ModaMiguelao.Web.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "DomainToViewModelMappings";
            }
        }

        ////testar obsoleto
        //public ViewModelToDomainMappingProfile()
        //{
        //    CreateMap<Client, ClientViewModel>();
        //    CreateMap<Product, ProductViewModel>();
        //    CreateMap<ShoppingCart, ShoppingCartViewModel>();
        //    CreateMap<ShoppingCartItem, ShoppingCartItemViewModel>();
        //    CreateMap<Order, OrderViewModel>();
        //    CreateMap<Address, AddressViewModel>();
        //    CreateMap<Administrator, AdministratorViewModel>();
        //}

        protected override void Configure()
        {
            CreateMap<Client, ClientViewModel>();
            CreateMap<Product, ProductViewModel>();
            CreateMap<ShoppingCart, ShoppingCartViewModel>();
            CreateMap<ShoppingCartItem, ShoppingCartItemViewModel>();
            CreateMap<Order, OrderViewModel>();
            CreateMap<Address, AddressViewModel>();
            CreateMap<Administrator, AdministratorViewModel>();
        }
    }
}
