﻿using SimpleInjector;

namespace ModaMiguelao.Web.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            /** Injeta as dependencias desse nível **/

            /** Injeta as dependencias das outras camadas **/
            ModaMiguelao.Application.IoC.BootStrapper.RegisterServices(container);
        }
    }
}
