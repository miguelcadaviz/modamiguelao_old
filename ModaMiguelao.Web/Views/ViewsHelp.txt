﻿/*
	Exibe o código html contido no parâmetro
*/
@Html.Raw(html)

/*
	Exibe o código html da action referida
	(tipo um partial)
*/
@Html.Action(action, controller, [parameters])