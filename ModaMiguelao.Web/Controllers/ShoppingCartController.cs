﻿using AutoMapper;
using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.PartialControllers;
using ModaMiguelao.Web.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ModaMiguelao.Web.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly IProductApplicationService _productApplication;

        public ShoppingCartController(IProductApplicationService productApplication)
        {
            _productApplication = productApplication;
        }

        public ViewResult Index(string returnUrl)
        {
            var _shoppingCart = GetShoppingCart();
            var shoppingCartVM = Mapper.Map<ShoppingCart, ShoppingCartViewModel>(_shoppingCart);

            ViewBag.returnUrl = returnUrl;
            ViewBag.urlRemoveitem = "/ShoppingCart/Remove/";
            ViewBag.Modal = new RemoveShoppingCartItemModalController();
            
            return View(shoppingCartVM);
        }

        public RedirectToRouteResult Add(long id, string returnUrl)
        {
            var product = _productApplication.GetById(id);

            if (product != null)
            {
                //ShoppingCart _shoppingCart = new ShoppingCart();
                var _shoppingCart = GetShoppingCart();

                _shoppingCart.AddItem(product);
                Session["ShoppingCart"] = _shoppingCart;
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        [HttpPost]
        public JsonResult Remove(long id)
        {
            System.Threading.Thread.Sleep(5000);

            string message = string.Empty;
            var _shoppingCart = GetShoppingCart();

            var _shoppingCartItem = _shoppingCart.Itens.Where(x => x.Product.Id == id).FirstOrDefault();
            if (_shoppingCartItem != null)
            {
                _shoppingCart.RemoveItem(_shoppingCartItem);
                Session["ShoppingCart"] = _shoppingCart;

                message = string.Format("{0} removido!", _shoppingCartItem.Product.Name);
            }


            return Json(new { Id = id, Message = message }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult Summary()
        {
            var shoppingCart = GetShoppingCart();
            var shoppingCartVM = Mapper.Map<ShoppingCart, ShoppingCartViewModel>(shoppingCart);

            return PartialView(shoppingCartVM);
        }


        public ShoppingCart GetShoppingCart()
        {
            ShoppingCart _shoppingCart = (ShoppingCart)Session["ShoppingCart"];
            if (_shoppingCart == null)
                _shoppingCart = new ShoppingCart();

            return _shoppingCart;
        }
    }
}