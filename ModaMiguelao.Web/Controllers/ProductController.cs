﻿using AutoMapper;
using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.PartialControllers;
using ModaMiguelao.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ModaMiguelao.Web.Controllers
{
    //TODO - Remover o entity da view

    public class ProductController : Controller
    {
        private readonly IProductApplicationService _productApplication;

        public ProductController(IProductApplicationService productApplication)
        {
            _productApplication = productApplication;
        }


        //TODO - diferença entre view result e action result
        public ViewResult Index(string category, int page = 1)
        {
            int productsPerPage = 5;

            var productsDomain = _productApplication
                .GetAll()
                .Where(x => string.IsNullOrEmpty(category) || x.Category == category)
                .OrderBy(p => p.Name)
                .Skip((page - 1) * productsPerPage)
                .Take(productsPerPage);

            var paging = new PagingController()
            {
                CurrentPage = page,
                ItensPerPage = productsPerPage,
                TotalItens = _productApplication
                    .GetAll()
                    .Where(x => string.IsNullOrEmpty(category) || x.Category == category)
                    .Count()
            };

            ViewBag.Paging = paging;
            ViewBag.CurrentCategory = category;

            var productsVM = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(productsDomain);

            return View(productsVM);
        }

        public PartialViewResult Category(string category = null)
        {
            ViewBag.CurrentCategory = category;

            IEnumerable<string> categories = _productApplication
                .GetAll()
                .Select(x=> x.Category)
                .Distinct()
                .OrderBy(x => x);

            return PartialView(categories);
        }
    }
}