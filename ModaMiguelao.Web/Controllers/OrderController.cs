﻿using AutoMapper;
using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.ViewModels;
using System.Web.Mvc;

namespace ModaMiguelao.Web.Controllers
{
    public class OrderController : Controller
    {
        private IOrderApplicationService OrderApplication { get; }

        private IProductApplicationService ProductApplication { get; }

        public OrderController(IOrderApplicationService orderApplication, IProductApplicationService productApplication)
        {
            OrderApplication = orderApplication;
            ProductApplication = productApplication;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ViewResult CloseOrder()
        {
            var orderVM = new OrderViewModel();
            var shoppingCart = GetShoppingCart();
            orderVM.ShoppingCart = Mapper.Map<ShoppingCart, ShoppingCartViewModel>(shoppingCart);

            return View(orderVM);
        }

        [HttpPost]
        public ViewResult CloseOrder(OrderViewModel orderVM)
        {
            var order = Mapper.Map<OrderViewModel, Order>(orderVM);

            if (ModelState.IsValid)
            {
                this.OrderApplication.CloseOrder(order);
                return View("CloseOrderSuccess");
            }

            return View(orderVM);
        }

        public ShoppingCart GetShoppingCart()
        {
            ShoppingCart _shoppingCart = (ShoppingCart)Session["ShoppingCart"];
            if (_shoppingCart == null)
                _shoppingCart = new ShoppingCart();

            return _shoppingCart;
        }
    }
}