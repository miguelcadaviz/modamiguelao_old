﻿using AutoMapper;
using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.PartialControllers;
using ModaMiguelao.Web.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ModaMiguelao.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly ILoginApplicationService _loginApplication;

        public AuthenticationController(ILoginApplicationService loginApplication)
        {
            _loginApplication = loginApplication;
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var login = new Administrator();
            var loginVM = Mapper.Map<Administrator, AdministratorViewModel>(login);

            return View(loginVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string returnUrl, AdministratorViewModel administrator)
        {
            ViewBag.ReturnUrl = returnUrl;

            if (!ModelState.IsValid)
                return View(administrator);

            var login = Mapper.Map<AdministratorViewModel, Administrator>(administrator);
            login = (Administrator)_loginApplication.GetById(1);
            if (login == null)
            {
                ModelState.AddModelError("", "");
            }
            else
            {
                FormsAuthentication.SetAuthCookie(login.User, true);

                //TODO is valid url
                if (Url.IsLocalUrl(returnUrl)
                    && returnUrl.Length > 1
                    && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//")
                    && !returnUrl.StartsWith("/\\"))

                    return Redirect(returnUrl);

                return RedirectToAction("Index", "Product", new { area = "Manager"});
            }

            return View(administrator);


        }

    }
}