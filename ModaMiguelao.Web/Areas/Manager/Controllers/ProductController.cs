﻿using AutoMapper;
using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ModaMiguelao.Web.Areas.Manager.Controllers
{
    //[Authorize]
    public class ProductController : Controller
    {
        private IProductApplicationService ProductApplication { get; }

        public ProductController(IProductApplicationService productApplication)
        {
            ProductApplication = productApplication;
        }

        public ActionResult Index()
        {
            var products = ProductApplication.GetAll();
            var productsVM = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

            return View(productsVM);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel productVM)
        {
            if (ModelState.IsValid)
            {
                var product = Mapper.Map<ProductViewModel, Product>(productVM);
                this.ProductApplication.Add(product);

                TempData["message"] = string.Format("{0} foi criado!", product.Name);
                return RedirectToAction("Index");
            }

            return View(productVM);
        }


        public ViewResult Edit(long id)
        {
            var product = this.ProductApplication.GetById(id);
            var productVM = Mapper.Map<Product, ProductViewModel>(product);

            return View(productVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel productVM)
        {
            if (ModelState.IsValid)
            {
                if(productVM.Image!= null)
                {
                    //faz upload da imagem
                }

                var product = Mapper.Map<ProductViewModel, Product>(productVM);
                this.ProductApplication.Update(product);

                TempData["message"] = string.Format("{0} foi alterado!", product.Name);
                return RedirectToAction("Index");
            }

            return View(productVM);
        }

        public ViewResult Delete(long id)
        {
            var product = this.ProductApplication.GetById(id);
            var productVM = Mapper.Map<Product, ProductViewModel>(product);

            return View(productVM);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            var product = ProductApplication.GetById(id);
            this.ProductApplication.Remove(product);

            TempData["message"] = string.Format("{0} foi removido!", product.Name);
            return RedirectToAction("Index");
        }




        //[HttpPost]
        //public JsonResult Edit(ProductViewModel productVM)
        //{
        //    string mensagem = string.Empty;
        //    return Json(mensagem, JsonRequestBehavior.AllowGet);
        //}

        //<input type="submit" class = "btn btn-default btn-xs" value="Excluir" id="@item.Id" name="@item.Nome" />
    }
}