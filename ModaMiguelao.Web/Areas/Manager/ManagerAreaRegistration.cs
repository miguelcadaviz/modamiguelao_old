﻿using System.Web.Mvc;

namespace ModaMiguelao.Web.Areas.Manager
{
    public class ManagerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Manager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Manager_default",
                url: "Manager/{controller}/{action}/{id}",
                defaults: new { controller="Product", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ModaMiguelao.Web.Areas.Manager.Controllers" }
            );
        }
    }
}