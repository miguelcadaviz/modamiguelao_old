﻿using ModaMiguelao.Infra.CrossCutting.Util.Extensions;
using ModaMiguelao.Infra.CrossCutting.Util.Specs;
using System;

namespace ModaMiguelao.Infra.CrossCutting.Util.ValueObjects
{
    public class CNPJ : ValueObject<CNPJ>
    {
        #region Fields
        public const int LENGTH = 14;
        private readonly string _cnpj;
        #endregion

        #region Constructors
        /// <summary>
        /// A new instance of the class <see cref="CNPJ"/>.
        /// </summary>
        /// <param name="cnpj">The CNPJ.</param>
        public CNPJ(string cnpj)
        {
            SpecService.Assert(new { CNPJ = cnpj }, new AllMustBeInformedSpec());
            this._cnpj = cnpj;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Obtém o CNPJ formatado.
        /// </summary>
        public string NumeroFormatado
        {
            get { return FormatarCNPJ(this._cnpj); }
        }

        /// <summary>
        /// Obtém o CNPJ com apenas números.
        /// </summary>
        public string Numero
        {
            get { return this._cnpj.OnlyNumbers(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Verifica se o CNPJ informado é válido.
        /// </summary>
        /// <returns>Retorna true caso seja válido.</returns>
        public bool IsValid()
        {
            if (this.NumeroFormatado == null)
                return false;

            string cnpj;
            string digitos;
            string digitosVerificadores;
            string digito1, digito2;
            int soma;
            int coeficiente;
            int resto;

            cnpj = Numero;

            //Validando o CNPJ
            if (cnpj.Length != 14)
            {
                return false;
            }

            // Caso coloque todos os numeros iguais
            if (cnpj == "00000000000000"
                || cnpj == "11111111111111"
                || cnpj == "22222222222222"
                || cnpj == "33333333333333"
                || cnpj == "44444444444444"
                || cnpj == "55555555555555"
                || cnpj == "66666666666666"
                || cnpj == "77777777777777"
                || cnpj == "88888888888888"
                || cnpj == "99999999999999"
                )
            {
                return false;
            }

            //Separando Digitos dos verificadores.
            digitos = cnpj.Substring(0, 12);
            digitosVerificadores = cnpj.Substring(12, 2);

            //Inicializando variáveis.
            soma = 0;
            coeficiente = 2;

            //Calculando o Primeiro digito Verificador.
            //Multiplica-se de tras pra frente pelo valor sequencial de 2 a 9.
            for (int i = 0; i < 12; i++)
            {
                soma += (Convert.ToInt32(cnpj.Substring((11 - i), 1)) * coeficiente);
                coeficiente += 1;
                //Caso o coeficiente supere nove deverá voltar ao valor de 2.
                if (coeficiente > 9)
                    coeficiente = 2;
            }
            //Calculando o resto.
            resto = soma % 11;
            //Caso o Resto seja menor que dois automaticamente
            //o Primeiro digito verificador será Zero.
            if (resto < 2)
            {
                digito1 = "0";
            }
            //Caso contrário basta subtrair o resto de 11.
            else
            {
                digito1 = Convert.ToString(11 - resto);
            }
            //Verifica se o primeiro dígito é igual ao fornecido
            if (digito1 != digitosVerificadores.Substring(0, 1))
                return false;

            //Concatenando o primeiro dígito,
            //pois o calculo do segundo depende dele
            digitos = digitos + digito1;
            //Zerando as variáveis.
            coeficiente = 2;
            soma = 0;
            //Calculando o segundo dígito verificador.
            //Mesmo calculo do primeiro com a diferença de que o loop
            //envolve o primeiro dígito verificador.
            for (int i = 0; i < 13; i++)
            {
                soma += (Convert.ToInt32(cnpj.Substring((12 - i), 1)) * coeficiente);
                coeficiente += 1;
                //Caso o coeficiente exceda 9 ele volta ao valor de 2.
                if (coeficiente > 9)
                    coeficiente = 2;
            }
            //Calcula o resto perante o divisor 11.
            resto = soma % 11;
            //Caso o resto seja menor que 2 o digito automaticamente será zero.
            if (resto < 2)
            {
                digito2 = "0";
            }
            //Caso contrário ele será calculado subtraindo de 11.
            else
            {
                digito2 = Convert.ToString((11 - resto));
            }
            //Confere se os verificadores batem.
            if (digito2 != digitosVerificadores.Substring(1, 1))
            {
                return false;
            }

            //Se for válido retorna true.
            return true;

        }

        private string FormatarCNPJ(string cnpj)
        {
            return Convert.ToDecimal(cnpj).ToString(@"00\.000\.000\/0000\-00");
        }
        #endregion

        #region Operators
        /// <summary>
        /// Performs an implicit conversion from <see cref="string"/> to <see cref="CNPJ"/>.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator CNPJ(string value)
        {
            return new CNPJ(value);
        }
        #endregion
    }
}
