﻿using ModaMiguelao.Infra.CrossCutting.Util.Specs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModaMiguelao.Infra.CrossCutting.Util.Extensions;
using System.Text.RegularExpressions;

namespace ModaMiguelao.Infra.CrossCutting.Util.ValueObjects
{
    /// <summary>
    /// ValueObject para os dados de CPF.
    /// </summary>
    public class CPF : ValueObject<CPF>
    {
        #region Fields
        public const int LENGTH = 11;
        private readonly string _cpf;
        #endregion

        #region Constructors
        /// <summary>
        /// Inicia uma nova instância da classe <see cref="CPF"/>.
        /// </summary>
        /// <param name="cpf">O cpf.</param>
        public CPF(string cpf)
        {
            SpecService.Assert(new { CPF = cpf }, new AllMustBeInformedSpec());
            this._cpf = cpf;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Obtém o CPF formatado.
        /// </summary>
        public string NumeroFormatado
        {
            get { return FormatarCPF(this._cpf); }
        }

        /// <summary>
        /// Obtém o CPF com apenas números.
        /// </summary>
        public string Numero
        {
            get { return _cpf.OnlyNumbers(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Verifica se o CPF informado é válido.
        /// </summary>
        /// <returns>Retorna true caso seja válido.</returns>
        public bool IsValid()
        {
            if (this.NumeroFormatado == null)
                return false;

            string strToCheck = this.NumeroFormatado;

            strToCheck = strToCheck.Replace("-", "");
            strToCheck = strToCheck.Replace(".", "");

            Regex reg = new Regex(@"(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)");
            if (!reg.IsMatch(strToCheck))
                return false;

            int d1, d2;
            int soma = 0;
            string digitado = "";
            string calculado = "";

            // Pesos para calcular o primeiro digito
            int[] peso1 = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            // Pesos para calcular o segundo digito
            int[] peso2 = new int[] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            int[] n = new int[11];

            // Limpa a string
            strToCheck = strToCheck.Replace(".", "").Replace("-", "").Replace("/", "").Replace("\\", "");

            // Se o tamanho for < 11 entao retorna como inválido
            if (strToCheck.Length != 11)
            {
                return false;
            }

            // Caso coloque todos os numeros iguais
            switch (strToCheck)
            {
                case "11111111111":
                case "00000000000":
                case "2222222222":
                case "33333333333":
                case "44444444444":
                case "55555555555":
                case "66666666666":
                case "77777777777":
                case "88888888888":
                case "99999999999":
                    return false;
            }

            try
            {
                // Quebra cada digito do CPF
                n[0] = Convert.ToInt32(strToCheck.Substring(0, 1));
                n[1] = Convert.ToInt32(strToCheck.Substring(1, 1));
                n[2] = Convert.ToInt32(strToCheck.Substring(2, 1));
                n[3] = Convert.ToInt32(strToCheck.Substring(3, 1));
                n[4] = Convert.ToInt32(strToCheck.Substring(4, 1));
                n[5] = Convert.ToInt32(strToCheck.Substring(5, 1));
                n[6] = Convert.ToInt32(strToCheck.Substring(6, 1));
                n[7] = Convert.ToInt32(strToCheck.Substring(7, 1));
                n[8] = Convert.ToInt32(strToCheck.Substring(8, 1));
                n[9] = Convert.ToInt32(strToCheck.Substring(9, 1));
                n[10] = Convert.ToInt32(strToCheck.Substring(10, 1));
            }
            catch
            {
                return false;
            }

            // Calcula cada digito com seu respectivo peso
            for (int i = 0; i <= peso1.GetUpperBound(0); i++)
            {
                soma += (peso1[i] * Convert.ToInt32(n[i]));
            }

            // Pega o resto da divisao
            int resto = soma % 11;

            if (resto == 1 || resto == 0)
            {
                d1 = 0;
            }
            else
            {
                d1 = 11 - resto;
            }

            soma = 0;

            // Calcula cada digito com seu respectivo peso
            for (int i = 0; i <= peso2.GetUpperBound(0); i++)
            {
                soma += (peso2[i] * Convert.ToInt32(n[i]));
            }

            // Pega o resto da divisao
            resto = soma % 11;

            if (resto == 1 || resto == 0)
            {
                d2 = 0;
            }
            else
            {
                d2 = 11 - resto;
            }

            calculado = d1.ToString() + d2.ToString();
            digitado = n[9].ToString() + n[10].ToString();

            // Se os ultimos dois digitos calculados bater com
            // os dois ultimos digitos do cpf entao é válido
            return calculado == digitado;
        }

        private string FormatarCPF(string cpf)
        {
            return Convert.ToDecimal(cpf).ToString(@"000\.000\.000\-00");
        }
        #endregion

        #region Operators
        /// <summary>
        /// Performs an implicit conversion from <see cref="string"/> to <see cref="CPF"/>.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator CPF(string value)
        {
            return new CPF(value);
        }
        #endregion
    }
}