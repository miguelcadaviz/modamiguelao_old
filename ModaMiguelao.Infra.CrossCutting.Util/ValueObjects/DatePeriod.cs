﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace ModaMiguelao.Infra.CrossCutting.Util.ValueObjects

{
    /// <summary>
    /// Defines the period between the start date and end date.
    /// </summary>
    [DebuggerDisplay("Start={Start}, End={End}")]
    public class DatePeriod : ValueObject<DatePeriod>
    {
        public static readonly DatePeriod Empty = new DatePeriod(DateTime.MinValue, DateTime.MinValue);

        public DatePeriod(DateTime start, DateTime end)
        {
            if (!IsValid(start, end))
            {
                //TODO: usar resource
                throw new InvalidOperationException("Start date should not be greater than end date.");
            }

            Start = start;
            End = end;
        }

        public DateTime Start { get; }

        public DateTime End { get; }

        public static bool operator ==(DatePeriod period1, DatePeriod period2)
        {
            return period1.Equals(period2);
        }

        public static bool operator !=(DatePeriod period1, DatePeriod period2)
        {
            return !period1.Equals(period2);
        }

        public static DatePeriod Parse(string startDate, string endDate)
        {
            return Parse(startDate, endDate, CultureInfo.CurrentCulture);
        }

        public static DatePeriod Parse(string startDate, string endDate, IFormatProvider formatter)
        {
            var start = DateTime.Parse(startDate, formatter);
            var end = DateTime.Parse(endDate, formatter);

            return new DatePeriod(start, end);
        }

        public static bool TryParse(string startDate, string endDate, out DatePeriod result)
        {
            return TryParse(startDate, endDate, CultureInfo.CurrentCulture, out result);
        }

        public static bool TryParse(string startDate, string endDate, IFormatProvider formatter, out DatePeriod result)
        {
            var isValid = false;
            var start = DateTime.Parse(startDate, formatter);
            var end = DateTime.Parse(endDate, formatter);
            result = Empty;

            if (IsValid(start, end))
            {
                isValid = true;
                result = new DatePeriod(start, end);
            }

            return isValid;
        }

        public static bool IsValid(DateTime start, DateTime end)
        {
            return start <= end;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is DatePeriod))
            {
                return false;
            }

            return Equals((DatePeriod)obj);
        }

        public override bool Equals(DatePeriod obj)
        {
            return obj.Start == Start && obj.End == End;
        }

        public override int GetHashCode()
        {
            return Start.GetHashCode() ^ End.GetHashCode();
        }
    }
}