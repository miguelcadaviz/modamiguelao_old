﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ModaMiguelao.Infra.CrossCutting.Util.Globalization
{
    /// <summary>
    /// Globalization helper.
    /// </summary>
    public static class GlobalizationHelper
    {
        #region Fields        
        private static CultureInfo[] _supportedCulture = new CultureInfo[]
        {
            new CultureInfo("pt-BR"),
            new CultureInfo("en-US"),
            new CultureInfo("es-ES")
        };
        #endregion

        #region Methods        
        public static ICollection<CultureInfo> GetAvailableCultures()
        {
            return _supportedCulture;
        }

        /// <summary>
        /// Gets the globalized text.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The globalized text.</returns>
        public static string GetText(object key)
        {
            return GetText(key, true);
        }

        /// <summary>
        /// Gets the globalized text.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="fallbackKey">The fallback key.</param>
        /// <returns>The globalized text.</returns>
        public static string GetText(object key, object fallbackKey)
        {
            var text = GetText(key, false);

            if (text == null)
            {
                text = GetText(fallbackKey);
            }

            return text;
        }

        /// <summary>
        /// Gets the globalized text.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="markNotFound">if set to <c>true</c> mark with "[TEXT NOT FOUND]" text if key was not found.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns>The globalized text.</returns>
        public static string GetText(object key, bool markNotFound, CultureInfo cultureInfo = null)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            var rm = Texts.ResourceManager;

            rm.IgnoreCase = true;
            var result = cultureInfo == null ? rm.GetString(key.ToString()) : rm.GetString(key.ToString(), cultureInfo);

            if (markNotFound && result == null)
            {
                result = $"[TEXT NOT FOUND] {key}";
            }

            return result;
        }

        /// <summary>
        /// For each culture.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="each">The each.</param>
        public static void ForEachCulture(object key, Action<CultureInfo, string> each)
        {
            if (each == null)
            {
                throw new ArgumentNullException("each");
            }

            foreach (var culture in _supportedCulture)
            {
                each(culture, GetText(key, true, culture));
            }
        }

        /// <summary>
        /// Gets the CultureInfo by native name.
        /// </summary>
        /// <param name="nativeName">Name of the native.</param>
        /// <returns>The CultureInfo.</returns>
        public static CultureInfo GetCultureInfoByNativeName(string nativeName)
        {
            return _supportedCulture
                .FirstOrDefault(f => f.NativeName.StartsWith(nativeName, StringComparison.OrdinalIgnoreCase) || f.EnglishName.StartsWith(nativeName, StringComparison.OrdinalIgnoreCase));
        }
        #endregion
    }
}
