﻿using ModaMiguelao.Infra.CrossCutting.Util.Extensions;
using ModaMiguelao.Infra.CrossCutting.Util.Globalization;
using System;
using System.Reflection;

namespace ModaMiguelao.Infra.CrossCutting.Util.Specs
{
    /// <summary>
    /// Especificação referente a o primeira propriedade não ser maior do que a segunda nem a segunda menor do que a primeira.
    /// </summary>    
    public class MustRespectRangeSpec : SpecBase<object>
    {
        /// <summary>
        /// Verifica se o alvo informado satisfaz a especificação.
        /// </summary>
        /// <param name="target">O alvo.</param>
        /// <returns>
        /// Se a especificação foi satisfeita pelo alvo.
        /// </returns>
        public override SpecResult IsSatisfiedBy(object target)
        {
            var allProperties = target == null ? new PropertyInfo[] { } : target.GetType().GetProperties();
            if (allProperties.Length > 1)
            {
                var val1 = allProperties[0].GetValue(target) as IComparable;
                var val2 = allProperties[1].GetValue(target) as IComparable;

                if (val1 != null && val2 != null && val1.CompareTo(val2) > 0)
                {
                    var msg = Texts.TheFieldMustNotBeLesserThanField.With(
                        GlobalizationHelper.GetText(allProperties[0].Name),
                        GlobalizationHelper.GetText(allProperties[1].Name));
                    return NotSatisfied(msg);
                }
            }

            return Satisfied();
        }
    }
}