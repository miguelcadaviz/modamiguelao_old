﻿using System;

namespace ModaMiguelao.Infra.CrossCutting.Util.Extensions
{
    public static class TimeSpanExtensions
    {
        private const string TIMEFORMAT = @"hh\:mm\:ss";
        private const string TIMEFORMATFORSTATISTICS = @"{0}h {1}min";
        private const string DAYSTIMEFORMAT = @"d\d\ " + TIMEFORMAT;

        // TODO: Validar esse metodo
        public static string ToTimeStampString(this TimeSpan value)
        {
            return value.ToString(value.Days > 0 ? DAYSTIMEFORMAT : TIMEFORMAT);
        }

        // TODO: Validar esse metodo
        public static string ToTimeSpanStringForStatistics(this TimeSpan value)
        {
            return string.Format(TIMEFORMATFORSTATISTICS, Math.Truncate(value.TotalHours), value.Minutes);
        }
    }
}
