﻿using ModaMiguelao.Infra.CrossCutting.Util.Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ModaMiguelao.Infra.CrossCutting.Util.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsEmpty(this ICollection input)
        {
            return input == null || input.Count == 0;
        }

        /// <summary>
        /// Verifica se o dado consta em uma determinada lista
        /// </summary>
        /// <typeparam name="T">Tipo do valor.</typeparam>
        /// <param name="value">Valor de referência.</param>
        /// <param name="list">Lista de valores para busca.</param>
        /// <returns>Verdadeiro, caso o valor esteja no conjunto.</returns>
        public static bool In<T>(this T value, params T[] list)
        {
            return list.Contains(value);
        }

        /// <summary>
        /// Verifica se o dado consta em uma determinada lista
        /// </summary>
        /// <typeparam name="T">Tipo do valor.</typeparam>
        /// <param name="value">Valor de referência.</param>
        /// <param name="comparer">Comparador do valor. Caso a comparação seja uma string, veja as opções de StringComparer.</param>
        /// <param name="list">Lista de valores para busca.</param>
        /// <returns>Verdadeiro, caso o valor esteja no conjunto.</returns>
        public static bool In<T>(this T value, IEqualityComparer<T> comparer, params T[] list)
        {
            comparer.ThrowIfNull("comparer");

            return list.Contains(value, comparer);
        }
    }
}
