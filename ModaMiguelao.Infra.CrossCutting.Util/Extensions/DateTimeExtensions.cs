﻿using System;

namespace ModaMiguelao.Infra.CrossCutting.Util.Extensions
{
    public static class DateTimeExtensions
    {
        private static readonly TimeSpan FinalDay = TimeSpan.FromDays(1).Add(TimeSpan.FromMilliseconds(-1));

        public static DateTime ToFinalDate(this DateTime date)
        {
            return date.Date.Add(FinalDay);
        }
    }
}
