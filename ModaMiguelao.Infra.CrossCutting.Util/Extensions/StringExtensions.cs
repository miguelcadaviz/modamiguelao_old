﻿using ModaMiguelao.Infra.CrossCutting.Util.Helpers;
using ModaMiguelao.Infra.CrossCutting.Util.Globalization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace ModaMiguelao.Infra.CrossCutting.Util.Extensions
{
    public static class StringExtensions
    {
        
        private const string PHONEFORMAT = "+## (##) #-####-####";
        private const string ONLYREGIONALANDPHONEREGEX = "^[0-9]{10,11}$";
        private const string ONLYPHONENUMBERREGEX = "^[0-9]{8,9}$";
        private const string ONLYDIGITS = @"[^0-9]";

        private const string DEFAULTCOUNTRYCODE = "55";
        private const string DEFAULTREGIONALCODE = "99";

        private static readonly Regex DigitsOnly = new Regex("[^\\d]", RegexOptions.Compiled);

        /// <summary>
        /// Formats the string into PhoneNumber standard format.
        /// e.g: "5551999223344" => "+55 (51) 9-9922-3344"
        /// </summary>
        /// <param name="unformattedNumber">Unformatted string to be formatted.</param>
        /// <returns>A formatted string following PhoneNumbers standard</returns>
        // TODO: Validar esse metodo
        public static string ToPhoneNumberFormat(this string unformattedNumber)
        {
            if (string.IsNullOrEmpty(unformattedNumber))
            {
                return unformattedNumber;
            }

            string sanitized = DigitsOnly.Replace(unformattedNumber ?? string.Empty, string.Empty);

            // flag to check whether it has country-code + regional-code or not.
            bool onlyPhoneNumber = Regex.IsMatch(sanitized, ONLYPHONENUMBERREGEX);
            bool onlyRegionalCodeAndPhone = Regex.IsMatch(sanitized, ONLYREGIONALANDPHONEREGEX);

            if (onlyPhoneNumber)
            {
                // .Insert(4, "-") will add - in the middle of the phone, even though it has 9 digits
                return string.Format("+{0}({1}) {2}", DEFAULTCOUNTRYCODE, DEFAULTREGIONALCODE, sanitized.Insert(4, "-"));
            }
            else if (onlyRegionalCodeAndPhone)
            {
                // gets regional code
                string regionalCode = sanitized.Substring(0, DEFAULTREGIONALCODE.Length);

                // removes regional code part, take the rest into phoneNumber variable
                string phoneNumber = sanitized.Remove(0, DEFAULTREGIONALCODE.Length);
                return string.Format("+{0}({1}) {2}", DEFAULTCOUNTRYCODE, regionalCode, phoneNumber.Insert(4, "-"));
            }

            bool nineDigitPhone = sanitized.Length == "#############".Length;
            return long.Parse(sanitized).ToString(nineDigitPhone ? string.Concat(PHONEFORMAT, "#") : PHONEFORMAT);
        }

        public static string UnescapeUnicode(this string encoded)
        {
            if (string.IsNullOrEmpty(encoded))
            {
                return string.Empty;
            }

            var encoder = Encoding.Unicode;
            var encodedValue = encoder.GetBytes(encoded);
            return encoder.GetString(encodedValue);
        }

        /// <summary>
        /// Convert numbers (0 or 1) and string ("true", "false") case-insensitive to boolean.
        /// </summary>
        /// <param name="value">Value to convert.</param>
        /// <returns></returns>
        // TODO: Validar esse metodo
        public static bool ToBoolean(this string value)
        {
            value.ThrowIfNull("value");

            if (value.Length > 1)
            {
                return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
            }

            if (!Char.IsNumber(value, 0))
            {
                throw new InvalidCastException(string.Format("Can't convert to boolean, value '{0}' is not 0 or 1.", value));
            }

            return Convert.ToBoolean(Int32.Parse(value, CultureInfo.InvariantCulture));
        }


        /// <summary>
        /// Removes whatever is not a number into a string
        /// </summary>
        /// <param name="value">Value to convert.</param>
        /// <returns>Return only numbers into a string.</returns>
        public static string OnlyNumbers(this string value)
        {
            return Regex.Replace(value, ONLYDIGITS, string.Empty);
        }

        /// <summary>
        /// Concatena os itens da lista como palavras, adicionando ',' entre os elementos e ' e ' entre o penúltimo e último elemento.
        /// </summary>
        /// <param name="words">As palavras.</param>
        /// <returns>As palavras concatenadas.</returns>
        public static string JoinWords(this IEnumerable<int> words)
        {
            if (words == null)
            {
                return string.Empty;
            }

            return words.Select(x => x.ToString(CultureInfo.InvariantCulture)).JoinWords();
        }
        /// <summary>
        /// Concatena os itens da lista como palavras, adicionando ',' entre os elementos e ' e ' entre o penúltimo e último elemento.
        /// </summary>
        /// <param name="words">As palavras.</param>
        /// <returns>As palavras concatenadas.</returns>
        public static string JoinWords(this IEnumerable<string> words)
        {
            return JoinWords(words, Texts.And);
        }

        /// <summary>
        /// Concatena os itens da lista como palavras, adicionando ',' entre os elementos e ' e ' entre o penúltimo e último elemento.
        /// </summary>
        /// <param name="words">As palavras.</param>
        /// <param name="lastSeparator">O último separador.</param>
        /// <returns>As palavras concatenadas.</returns>
        public static string JoinWords(this IEnumerable<string> words, string lastSeparator)
        {
            if (words == null)
                return string.Empty;

            var lastWord = words.LastOrDefault();
            var wordsCount = words.Count();

            if (wordsCount == 1)
                return lastWord;

            var firstWords = words.Take(wordsCount - 1);

            return $"{string.Join(", ", firstWords)} {lastSeparator} {lastWord}";
        }


        //TODO: será q precisa?
        /// <summary>
        /// Formata a string informada utilizando InvariantCulture.
        /// </summary>
        /// <param name="value">A string.</param>
        /// <param name="args">Os argumentos para formatação da string.</param>
        /// <returns>A string formatada.</returns>
        public static string With(this string value, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, value, args);
        }
    }
}
