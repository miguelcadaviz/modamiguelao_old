﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModaMiguelao.Infra.CrossCutting.Util.Helpers
{
    /// <summary>
    /// Helper para exceções.
    /// </summary>
    public static class ExceptionHelper
    {
        /// <summary>
        /// Levanta <see cref="ArgumentNullException"/> para o parâmetro em argumentName, caso o valor em argumentValue seja null.
        /// </summary>
        /// <param name="argumentValue">O valor do parâmetro.</param>
        /// <param name="argumentName">O nome do parâmetro.</param>
        public static void ThrowIfNull(this object argumentValue, string argumentName )
        {
            if (argumentValue == null )
            {
                throw new ArgumentNullException(argumentName);
            }
        }

        /// <summary>
        /// Obtém a exception formatada.
        /// </summary>
        /// <param name="ex">A exceção.</param>
        /// <param name="crlf">O crlf.</param>
        /// <returns>A exceção formatada.</returns>
        public static string GetFormattedException(Exception ex, string crlf = null)
        {
            crlf = crlf ?? Environment.NewLine;

            var exception = new StringBuilder();

            exception.Append($"MESSAGE:{crlf}");
            exception.Append(ex.Message.Replace("\n", crlf));
            exception.Append($"{crlf}{crlf}");

            if (ex.InnerException != null)
            {
                exception.Append($"INNEREXCEPTION:{crlf}");
                exception.Append(GetFormattedException(ex.InnerException, crlf));
                exception.Append($"{crlf}{crlf}");
            }

            exception.Append($"SOURCE:{crlf}");
            exception.Append(ex.Source);
            exception.Append($"{crlf}{crlf}");

            exception.Append($"STACKTRACE:{crlf}");
            exception.Append(ex.StackTrace);
            exception.Append($"{crlf}{crlf}");

            exception.Append($"TARGETSITE:{crlf}");
            exception.Append(ex.TargetSite);
            exception.Append($"{crlf}{crlf}");

            return exception.ToString();
        }

    }
}
