﻿using System.Collections.Generic;
using System.Net.Mail;

namespace ModaMiguelao.Infra.CrossCutting.Util.Service
{
    public interface IEmailService
    {
        bool Send(MailAddress from, List<MailAddress> to, string subject, string body);
        bool Send(MailAddress from, List<MailAddress> to, List<MailAddress> cc, List<MailAddress> bcc, string subject, string body, List<string> attachmentFilenames);
    }
}
