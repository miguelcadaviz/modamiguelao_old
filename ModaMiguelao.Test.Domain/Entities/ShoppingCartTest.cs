﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModaMiguelao.Domain.Entities;
using System.Linq;

namespace ModaMiguelao.Test.Domain.Entities
{
    [TestClass]
    public class ShoppingCartTest
    {
        ShoppingCart _shoppingCart;

        public ShoppingCartTest()
        {
            _shoppingCart = new ShoppingCart();
        }

        [TestMethod]
        public void AddItens()
        {
            //Obtém produtos a serem inseridos
            var product1 = ProductTest.GetProduct1();
            var product2 = ProductTest.GetProduct2();

            //Adiciona o produto1
            _shoppingCart.AddItem(product1);
            Assert.AreEqual(1, _shoppingCart.Itens.Count, "Quantidade diferente da esperada!");

            //Adiciona mais algumas quantidades ao produto1
            _shoppingCart.AddItem(product1, 2);
            Assert.AreEqual(1, _shoppingCart.Itens.Count, "Quantidade diferente da esperada!");
            Assert.AreEqual(3, _shoppingCart.Itens.Where(x => x.Product.Id == product1.Id).FirstOrDefault().Quantity, "Quantidade diferente da esperada!");

            //Adiciona o produto2
            _shoppingCart.AddItem(product2, 4);
            Assert.AreEqual(2, _shoppingCart.Itens.Count, "Quantidade diferente da esperada!");
            Assert.AreEqual(4, _shoppingCart.Itens.Where(x => x.Product.Id == product2.Id).FirstOrDefault().Quantity, "Quantidade diferente da esperada!");
        }

        [TestMethod]
        public void RemoveItens()
        {
            //Obtém produtos a serem inseridos/removidos
            var product1 = ProductTest.GetProduct1();
            var product2 = ProductTest.GetProduct2();

            //Adiciona os produtos
            _shoppingCart.AddItem(product1);
            _shoppingCart.AddItem(product2, 4);

            //Seleciona o item a ser removido
            var shoppingCartItem = _shoppingCart.Itens.Find(x => x.Product.Id == product1.Id);
            _shoppingCart.RemoveItem(shoppingCartItem);

            Assert.AreEqual(1, _shoppingCart.Itens.Count, "Quantidade diferente da esperada!");
        }

        [TestMethod]
        public void Clear()
        {
            //Obtém produtos a serem inseridos
            var product1 = ProductTest.GetProduct1();
            var product2 = ProductTest.GetProduct2();

            //Adiciona os produtos
            _shoppingCart.AddItem(product1);
            _shoppingCart.AddItem(product2, 4);

            //Limpa o carrinho
            _shoppingCart.Clear();

            Assert.AreEqual(0, _shoppingCart.Itens.Count, "Quantidade diferente da esperada!");
        }

        [TestMethod]
        public void Total()
        {
            //Obtém produtos a serem inseridos
            var product1 = ProductTest.GetProduct1();
            int product1Quantity = 2;
            var product2 = ProductTest.GetProduct2();
            int product2Quantity = 4;

            //Calcula o total
            decimal total = 0;
            total += product1.Value * product1Quantity;
            total += product2.Value * product2Quantity;

            //Adiciona os produtos
            _shoppingCart.AddItem(product1, product1Quantity);
            _shoppingCart.AddItem(product2, product2Quantity);

            Assert.AreEqual(total, _shoppingCart.Total, "Valor diferente do esperado!");

            //Remove um item e recalcula o total
            _shoppingCart.RemoveItem(_shoppingCart.Itens.FirstOrDefault());
            total -= product1.Value * product1Quantity;

            Assert.AreEqual(total, _shoppingCart.Total, "Valor diferente do esperado!");
        }
    }
}
