﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Test.Domain.Entities
{
    [TestClass]
    public class ProductTest
    {

        public static Product GetProduct1()
        {
            return new Product()
            {
                Name = "Product1",
                Id = 1,
                Value = 1.99m,
            };
        }

        public static Product GetProduct2()
        {
            return new Product()
            {
                Name = "Product2",
                Id = 2,
                Value = 10.99m,
            };
        }

    }
}
