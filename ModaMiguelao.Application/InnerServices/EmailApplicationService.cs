﻿using ModaMiguelao.Domain.Entities;
using System;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace ModaMiguelao.Application.InnerServices
{
    internal class EmailApplicationService
    {
        private SmtpClient SmtpClient { get; }

        public EmailApplicationService()
        {
            this.SmtpClient = new SmtpClient();
        }

        public void SendNewOrderEmail(Order order)
        {
            var email = NewMailMessage();

            email.Subject = "Comprou";
            email.Body = "tu comprou coisa!";

            email.To.Add(new MailAddress(order.Client.Email, order.Client.Name));

            //var attachment = AddAttachment(string.Empty);
            //if(attachment != null)
            //    email.Attachments.Add(attachment);

            this.SmtpClient.Send(email);
        }

        private MailMessage NewMailMessage()
        {
            return new MailMessage()
            {
                IsBodyHtml = true,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                BodyEncoding = Encoding.GetEncoding("ISO-8859-1"),
            };
        }

        private Attachment AddAttachment(string attachmentPath)
        {
            if (attachmentPath.Length <= attachmentPath.LastIndexOf("\\", StringComparison.Ordinal) + 1)
                return null;

            var fileName = attachmentPath.Substring(attachmentPath.LastIndexOf("\\", StringComparison.Ordinal) + 1);

            var file = new FileStream(attachmentPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            return new Attachment(file, fileName);
        }
    }
}
