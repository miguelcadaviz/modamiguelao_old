﻿using ModaMiguelao.Application.InnerServices;
using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Application.Services
{
    internal class OrderApplicationService : ApplicationServiceBase<Order>, IOrderApplicationService
    {
        #region Properties
        private IOrderService OrderService { get; }
        #endregion

        #region Constructors
        public OrderApplicationService(IOrderService orderService) : base(orderService)
        {
            OrderService = orderService;
        }
        #endregion

        #region Methods
        #region IOrderApplicationService implementation
        public void CloseOrder(Order order)
        {
            //Paga

            //Confirma os produtos

            //Envia o email
            new EmailApplicationService().SendNewOrderEmail(order);


        }
        #endregion
        #endregion
    }
}
