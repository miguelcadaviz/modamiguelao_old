﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace ModaMiguelao.Application.Services
{
    internal class ApplicationServiceBase<TEntity> : IApplicationServiceBase<TEntity>, IDisposable where TEntity : class
    {
        #region Properties
        private IServiceBase<TEntity> ServiceBase { get; }
        #endregion

        #region Constructors
        public ApplicationServiceBase(IServiceBase<TEntity> serviceBase)
        {
            ServiceBase = serviceBase;
        }
        #endregion

        #region Methods
        #region IApplicationServiceBase implementation
        public void Add(TEntity item)
        {
            ServiceBase.Add(item);
        }
        public void Update(TEntity item)
        {
            ServiceBase.Update(item);
        }
        public void Remove(TEntity item)
        {
            ServiceBase.Remove(item);
        }
        public TEntity GetById(long id)
        {
            return ServiceBase.GetById(id);
        }
        public IEnumerable<TEntity> GetAll()
        {
            return ServiceBase.GetAll();
        }
        #endregion

        #region IDisposable implementation
        public void Dispose()
        {
            ServiceBase.Dispose();
        }
        #endregion
        #endregion
    }
}
