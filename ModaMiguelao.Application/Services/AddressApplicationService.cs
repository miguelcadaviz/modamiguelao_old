﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Application.Services
{
    internal class AddressApplicationService : ApplicationServiceBase<Address>, IAddressApplicationService
    {
        #region Properties
        private IAddressService AddressService { get; }
        #endregion

        #region Constructors
        public AddressApplicationService(IAddressService addressService) : base(addressService)
        {
            AddressService = addressService;
        }
        #endregion

        #region Methods
        #region IAddressApplicationService implementation
        #endregion
        #endregion
    }
}
