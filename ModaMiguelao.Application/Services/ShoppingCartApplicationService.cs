﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Application.Services
{
    internal class ShoppingCartApplicationService : ApplicationServiceBase<ShoppingCart>, IShoppingCartApplicationService
    {
        #region Properties
        private IShoppingCartService ShoppingCartService { get; }
        private IShoppingCartItemService ShoppingCartItemService { get; }
        #endregion

        #region Constructors
        public ShoppingCartApplicationService(IShoppingCartService shoppingCartService, IShoppingCartItemService shoppingCartItemService) 
            : base(shoppingCartService)
        {
            ShoppingCartService = shoppingCartService;
            ShoppingCartItemService = shoppingCartItemService;
        }
        #endregion

        #region Methods
        #region IShoppingCartApplicationService implementation
        #endregion
        #endregion
    }
}
