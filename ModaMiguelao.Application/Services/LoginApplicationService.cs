﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Application.Services
{
    internal class LoginApplicationService: ApplicationServiceBase<Login>, ILoginApplicationService
    {
        #region Properties
        private ILoginService LoginService { get; }
        #endregion

        #region Constructors
        public LoginApplicationService(ILoginService loginService) : base(loginService)
        {
            LoginService = loginService;
        }
        #endregion

        #region Methods
        #region ILoginApplicationService implementation
        #endregion
        #endregion

    }
}
