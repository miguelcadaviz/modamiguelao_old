﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Services;

namespace ModaMiguelao.Application.Services
{
    internal class ClientApplicationService : ApplicationServiceBase<Client>, IClientApplicationService
    {
        #region Properties
        private IClientService ClientService { get; }
        #endregion

        #region Constructors
        public ClientApplicationService(IClientService clientService) : base(clientService)
        {
            ClientService = clientService;
        }
        #endregion

        #region Methods
        #region IClientApplicationService implementation
        #endregion
        #endregion

        //public IEnumerable<Client> GetSpecialClients()
        //{
        //    return _clientService.GetSpecialClients(_clientService.GetAll());
        //}
    }
}
