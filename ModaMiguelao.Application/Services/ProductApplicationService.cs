﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace ModaMiguelao.Application.Services
{
    internal class ProductApplicationService : ApplicationServiceBase<Product>, IProductApplicationService
    {
        #region Properties
        private IProductService ProductService { get; }
        #endregion

        #region Constructors
        public ProductApplicationService(IProductService productService) : base(productService)
        {
            ProductService = productService;
        }
        #endregion

        #region Methods
        #region IProductApplicationService implementation
        public IEnumerable<Product> GetByName(string name)
        {
            return ProductService.GetByName(name);
        }
        #endregion
        #endregion
    }
}
