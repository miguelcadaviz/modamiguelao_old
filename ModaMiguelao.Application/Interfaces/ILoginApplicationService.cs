﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Application.Interfaces
{
    public interface ILoginApplicationService : IApplicationServiceBase<Login>
    {
    }
}
