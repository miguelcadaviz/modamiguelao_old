﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Application.Interfaces
{
    public interface IShoppingCartApplicationService : IApplicationServiceBase<ShoppingCart>
    {
    }
}
