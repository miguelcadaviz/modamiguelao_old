﻿using ModaMiguelao.Domain.Entities;
using System.Collections.Generic;

namespace ModaMiguelao.Application.Interfaces
{
    public interface IClientApplicationService : IApplicationServiceBase<Client>
    {
        //IEnumerable<Client> GetSpecialClients();
    }
}
