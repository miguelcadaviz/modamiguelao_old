﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Application.Interfaces
{
    public interface IOrderApplicationService : IApplicationServiceBase<Order>
    {
        void CloseOrder(Order order);
    }
}
