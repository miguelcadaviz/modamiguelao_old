﻿using ModaMiguelao.Domain.Entities;

namespace ModaMiguelao.Application.Interfaces
{
    public interface IAddressApplicationService : IApplicationServiceBase<Address>
    {
    }
}
