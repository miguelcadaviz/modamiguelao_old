﻿using ModaMiguelao.Domain.Entities;
using System.Collections.Generic;

namespace ModaMiguelao.Application.Interfaces
{
    public interface IProductApplicationService : IApplicationServiceBase<Product>
    {
        IEnumerable<Product> GetByName(string name);
    }
}
