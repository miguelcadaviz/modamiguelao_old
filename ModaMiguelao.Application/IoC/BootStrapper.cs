﻿using ModaMiguelao.Application.Interfaces;
using ModaMiguelao.Application.Services;
using SimpleInjector;

namespace ModaMiguelao.Application.IoC
{
    public  class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            /** Injeta as dependencias desse nível **/
            container.Register(typeof(IApplicationServiceBase<>), typeof(ApplicationServiceBase<>), Lifestyle.Scoped);
            container.Register<IProductApplicationService, ProductApplicationService>(Lifestyle.Scoped);
            container.Register<IClientApplicationService, ClientApplicationService>(Lifestyle.Scoped);
            container.Register<IAddressApplicationService, AddressApplicationService>(Lifestyle.Scoped);
            container.Register<IOrderApplicationService, OrderApplicationService>(Lifestyle.Scoped);
            container.Register<IShoppingCartApplicationService, ShoppingCartApplicationService>(Lifestyle.Scoped);
            container.Register<ILoginApplicationService, LoginApplicationService>(Lifestyle.Scoped);

            /** Injeta as dependencias das outras camadas **/
            //Dependencias de Domínio
            ModaMiguelao.Domain.IoC.BootStrapper.RegisterServices(container);
            //Dependencias de Banco
            ModaMiguelao.Infra.Data.IoC.BootStrapper.RegisterServices(container);
        }
    }
}
