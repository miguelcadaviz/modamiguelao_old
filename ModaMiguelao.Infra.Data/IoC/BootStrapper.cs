﻿using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Infra.Data.Repositories;
using SimpleInjector;

namespace ModaMiguelao.Infra.Data.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            /** Injeta as dependencias desse nível **/
            container.Register(typeof(IRepositoryBase<>), typeof(RepositoryBase<>), Lifestyle.Scoped);
            container.Register<IClientRepository, ClientRepository>(Lifestyle.Scoped);
            container.Register<IProductRepository, ProductRepository>(Lifestyle.Scoped);
            container.Register<IAddressRepository, AddressRepository>(Lifestyle.Scoped);
            container.Register<IOrderRepository, OrderRepository>(Lifestyle.Scoped);
            container.Register<IShoppingCartItemRepository, ShoppingCartItemRepository>(Lifestyle.Scoped);
            container.Register<IShoppingCartRepository, ShoppingCartRepository>(Lifestyle.Scoped);
            container.Register<IAdministratorRepository, AdministratorRepository>(Lifestyle.Scoped);
            container.Register<ILoginRepository, LoginRepository>(Lifestyle.Scoped);

            /** Injeta as dependencias das outras camadas **/
        }
    }
}
