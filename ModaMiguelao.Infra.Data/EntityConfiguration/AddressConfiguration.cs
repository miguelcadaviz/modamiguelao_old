﻿using ModaMiguelao.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModaMiguelao.Infra.Data.EntityConfiguration
{
    internal class AddressConfiguration : EntityTypeConfiguration<Address>
    {
        public AddressConfiguration()
        {
            HasKey(c => c.Id);
        }
    }
}
