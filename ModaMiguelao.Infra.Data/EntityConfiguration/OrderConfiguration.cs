﻿using ModaMiguelao.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModaMiguelao.Infra.Data.EntityConfiguration
{
    internal class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            HasKey(c => c.Id);
        }
    }
}
