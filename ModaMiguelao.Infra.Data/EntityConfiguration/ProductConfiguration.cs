﻿using ModaMiguelao.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModaMiguelao.Infra.Data.EntityConfiguration
{
    internal class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            //ToTable("Odara");

            HasKey(p => p.Id);

            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(250);

            Property(p => p.Value)
                .IsRequired();

            //HasRequired(p => p.Client)
            //    .WithMany()
            //    .HasForeignKey(p => p.ClientId);
        }
    }
}
