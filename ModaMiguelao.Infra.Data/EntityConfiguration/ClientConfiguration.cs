﻿using ModaMiguelao.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModaMiguelao.Infra.Data.EntityConfiguration
{
    internal class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(150);

            Property(c => c.Surname)
                .IsRequired()
                .HasMaxLength(150);

            Property(c => c.Email)
                .IsRequired();
        }
    }
}
