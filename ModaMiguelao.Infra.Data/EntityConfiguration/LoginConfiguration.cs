﻿using ModaMiguelao.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ModaMiguelao.Infra.Data.EntityConfiguration
{
    internal class LoginConfiguration : EntityTypeConfiguration<Login>
    {
        public LoginConfiguration()
        {
            ToTable("Users");

            HasKey(c => c.Id);

            Property(c => c.User)
                .IsRequired()
                .HasMaxLength(30);

            Property(c => c.Password)
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(128);

            Property(c => c.LastAccess)
                .IsRequired();

            Property(c => c.Active)
                .IsRequired();
        }
    }
}
