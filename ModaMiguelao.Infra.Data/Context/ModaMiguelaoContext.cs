﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Infra.Data.EntityConfiguration;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace ModaMiguelao.Infra.Data.Context
{
    internal class ModaMiguelaoContext : DbContext
    {
        public ModaMiguelaoContext() : base("ModaMiguelao") { }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Login> Logins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configuração de convenções
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            //Configuração default de tabelas
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new LoginConfiguration());


            //Defaults para campos de banco por tipo
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

        }

        public override int SaveChanges()
        {
            //TODO - criar nome padrão para "data inclusao"
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("ClientSince") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("ClientSince").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("ClientSince").IsModified = false;
                }
            }

            return base.SaveChanges();
        }
    }
}
