namespace ModaMiguelao.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Client", "Address_Id", "dbo.Address");
            DropIndex("dbo.Client", new[] { "Address_Id" });
            DropColumn("dbo.Client", "Address_Id");
            DropTable("dbo.Address");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AddressType = c.Int(nullable: false),
                        ZipCode = c.String(maxLength: 100, unicode: false),
                        Street = c.String(maxLength: 100, unicode: false),
                        District = c.String(maxLength: 100, unicode: false),
                        City = c.String(maxLength: 100, unicode: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Client", "Address_Id", c => c.Long());
            CreateIndex("dbo.Client", "Address_Id");
            AddForeignKey("dbo.Client", "Address_Id", "dbo.Address", "Id");
        }
    }
}
