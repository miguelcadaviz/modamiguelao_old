namespace ModaMiguelao.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class upd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ZipCode = c.String(maxLength: 100, unicode: false),
                        Street = c.String(maxLength: 100, unicode: false),
                        District = c.String(maxLength: 100, unicode: false),
                        City = c.String(maxLength: 100, unicode: false),
                        State = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Client", "Address_Id", c => c.Long());
            CreateIndex("dbo.Client", "Address_Id");
            AddForeignKey("dbo.Client", "Address_Id", "dbo.Address", "Id");
            DropColumn("dbo.Client", "Address_ZipCode");
            DropColumn("dbo.Client", "Address_Street");
            DropColumn("dbo.Client", "Address_District");
            DropColumn("dbo.Client", "Address_City");
            DropColumn("dbo.Client", "Address_State");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Client", "Address_State", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_City", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_District", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_Street", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_ZipCode", c => c.String(maxLength: 100, unicode: false));
            DropForeignKey("dbo.Client", "Address_Id", "dbo.Address");
            DropIndex("dbo.Client", new[] { "Address_Id" });
            DropColumn("dbo.Client", "Address_Id");
            DropTable("dbo.Address");
        }
    }
}
