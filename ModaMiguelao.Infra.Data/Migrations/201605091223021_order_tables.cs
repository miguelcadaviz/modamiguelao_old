namespace ModaMiguelao.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class order_tables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Client", "Address_ZipCode", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_Street", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_District", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_City", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.Client", "Address_State", c => c.String(maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Client", "Address_State");
            DropColumn("dbo.Client", "Address_City");
            DropColumn("dbo.Client", "Address_District");
            DropColumn("dbo.Client", "Address_Street");
            DropColumn("dbo.Client", "Address_ZipCode");
        }
    }
}
