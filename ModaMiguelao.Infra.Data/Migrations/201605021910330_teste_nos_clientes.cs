namespace ModaMiguelao.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste_nos_clientes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Client", "MiddleName", c => c.String(maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Client", "MiddleName");
        }
    }
}
