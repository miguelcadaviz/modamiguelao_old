namespace ModaMiguelao.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Address", "AddressType", c => c.Int(nullable: false));
            AlterColumn("dbo.Address", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Address", "State", c => c.String(maxLength: 100, unicode: false));
            DropColumn("dbo.Address", "AddressType");
        }
    }
}
