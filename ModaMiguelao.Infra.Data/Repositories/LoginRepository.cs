﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;

namespace ModaMiguelao.Infra.Data.Repositories
{
   internal class LoginRepository : RepositoryBase<Login>, ILoginRepository
    {
    }
}
