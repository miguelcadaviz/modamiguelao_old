﻿using ModaMiguelao.Domain.Interfaces.Repositories;
using ModaMiguelao.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ModaMiguelao.Infra.Data.Repositories
{
    internal class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected ModaMiguelaoContext Db = new ModaMiguelaoContext();

        public void Add(TEntity item)
        {
            Db.Set<TEntity>().Add(item);
            Db.SaveChanges();
        }
        public void Update(TEntity item)
        {
            Db.Entry(item).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Remove(TEntity item)
        {
            Db.Set<TEntity>().Remove(item);
            Db.SaveChanges();
        }

        public TEntity GetById(long id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
            // TODO - pesquisar NoTracking
            //return Db.Set<TEntity>().AsNoTracking().ToList();
        }

        public void Dispose()
        {

        }
    }
}
