﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;

namespace ModaMiguelao.Infra.Data.Repositories
{
    internal class ClientRepository : RepositoryBase<Client>, IClientRepository
    {
    }
}
