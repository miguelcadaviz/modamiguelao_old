﻿using ModaMiguelao.Domain.Entities;
using ModaMiguelao.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace ModaMiguelao.Infra.Data.Repositories
{
    internal class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public IEnumerable<Product> GetByName(string name)
        {
            return Db.Products
                .Where(item => item.Name == name);
        }
    }
}
